import './App.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPaperPlane, faUser } from '@fortawesome/free-solid-svg-icons';
import { useEffect, useRef, useState } from 'react';
import { io } from 'socket.io-client';

const socket = io('http://localhost:3000');

function App() {
  const [name, setName] = useState('anonymous');
  const [message, setMessage] = useState('');
  const [feedback, setFeedback] = useState('');
  const [messages, setMessages] = useState([]);
  const [userCount, setUserCount] = useState(0);
  const [userList, setUserList] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [privateMessages, setPrivateMessages] = useState({});
  const [connectedUsers, setConnectedUsers] = useState([]);
  const typingTimeoutRef = useRef(null);

  useEffect(() => {
    socket.emit('setUsername', name);

    socket.on('message', (message) => {
      setMessages((prevMessages) => [...prevMessages, message]);
    });

    socket.on('privateMessage', ({ senderId, message }) => {
      setPrivateMessages((prevPrivateMessages) => {
        const userMessages = prevPrivateMessages[senderId] || [];
        return {
          ...prevPrivateMessages,
          [senderId]: [...userMessages, message],
        };
      });
    });

    socket.on('updateUserList', (users) => {
      setUserList(users);
    });

    socket.on('userCount', (userTotal) => {
      setUserCount(userTotal);
    });

    socket.on('typing', (user) => {
      setFeedback(`${user} is typing...`);
    });

    socket.on('stopTyping', () => {
      setFeedback('');
    });

    socket.on('connectedUsers', (users) => {
      setConnectedUsers(users);
    });

    return () => {
      socket.off('message');
      socket.off('userCount');
      socket.off('typing');
      socket.off('stopTyping');
      socket.off('updateUserList');
      socket.off('privateMessage');
      socket.off('connectedUsers');
    };
  }, [messages, feedback, userList]);

  const handleNameChange = (e) => {
    setName(e.target.value);
    socket.emit('setUsername', e.target.value);
  };

  const handleMessageChange = (e) => {
    setMessage(e.target.value);

    if (typingTimeoutRef.current) {
      clearTimeout(typingTimeoutRef.current);
    }

    socket.emit('typing', { recipientId: selectedUser, user: name });

    typingTimeoutRef.current = setTimeout(() => {
      socket.emit('stopTyping', selectedUser);
    }, 1000);
  };

  const handleMessageSend = (e) => {
    e.preventDefault();
    const newMessage = {
      text: message,
      author: name,
      date: new Date().toLocaleString(),
      senderId: socket.id,
    };

    if (selectedUser) {
      socket.emit('privateMessage', {
        recipientId: selectedUser,
        message: newMessage,
      });
      setPrivateMessages((prevPrivateMessages) => {
        const userMessages = prevPrivateMessages[selectedUser] || [];
        return {
          ...prevPrivateMessages,
          [selectedUser]: [...userMessages, newMessage],
        };
      });
    } else {
      socket.emit('message', newMessage);
    }

    setMessage('');
    socket.emit('stopTyping', selectedUser);
  };

  const handleUserClick = (userId) => {
    setSelectedUser(userId === selectedUser ? null : userId);
  };

  useEffect(() => {
    return () => {
      if (typingTimeoutRef.current) {
        clearTimeout(typingTimeoutRef.current);
      }
    };
  }, []);

  return (
    <>
      <h1 className='title'>iChat</h1>
      <div className="mainChat">
        <div className="flex">
          <div className="userList">
            <h3>Users : {userCount}</h3>
            <ul>
              <li onClick={() => handleUserClick(null)} className={!selectedUser ? 'selectedUser' : ''}>All</li>
              {Object.keys(userList).filter(user => user !== socket.id).map((user, index) => (
                <li
                  key={index}
                  className={selectedUser === user ? 'selectedUser' : ''}
                  onClick={() => handleUserClick(user)}
                >
                  {userList[user]}
                </li>
              ))}
            </ul>
          </div>
          <div className="chat">
            <div className="name">
              <span className="nameForm">
                <FontAwesomeIcon icon={faUser} />
                <input
                  type="text"
                  className="nameInput"
                  id="nameInput"
                  value={name}
                  onChange={handleNameChange}
                  maxLength="20"
                />
              </span>
            </div>
            <div className="chatHeader">
              {selectedUser && (
                <div className="selectedUserInfo">
                  <span>{userList[selectedUser]} {connectedUsers.includes(selectedUser) ? '(en ligne)' : '(hors ligne)'}</span>
                </div>
              )}
            </div>
            <ul className="conversation">
              {(selectedUser ? privateMessages[selectedUser] : messages)?.map((msg, index) => (
                <li key={index} className={msg.senderId === socket.id ? 'messageRight' : 'messageLeft'}>
                  <p className="message">{msg.text}</p>
                  <span>{msg.author} - {msg.date}</span>
                </li>
              ))}
              {feedback && (
                <li className="messageFeedback">
                  <p className="feedback">{feedback}</p>
                </li>
              )}
            </ul>
            <form className="messageForm" onSubmit={handleMessageSend}>
              <input
                type="text"
                name="message"
                className='messageInput'
                value={message}
                onKeyUp={() => {
                  if (!message) {
                    socket.emit('stopTyping', selectedUser);
                  }
                }}
                onChange={handleMessageChange}
              />
              <div className="vDivider"></div>
              <button type="submit" className='sendButton'>Send <FontAwesomeIcon icon={faPaperPlane} /></button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
