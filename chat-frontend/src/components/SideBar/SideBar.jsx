import * as React from 'react';
import Box from '@mui/material/Box';
import { Badge, Button, Drawer, List, ListItem, ListItemText, } from '@mui/material';
import ListItemButton from '@mui/material/ListItemButton';
import MailIcon from '@mui/icons-material/Mail';
import PersonIcon from '@mui/icons-material/Person';
import Person from '@mui/icons-material/Person';



export default function SideBar() {
  const [open, setOpen] = React.useState(false);

  const toggleDrawer = (newOpen) => () => {
    setOpen(newOpen);
  };


  const DrawerList = (
    <Box sx={{ width: 250 }} role="presentation" onClick={toggleDrawer(false)}>
      <List>
        {['Comlan', 'Beh', 'Ardieu', 'ALOTIN'].map((text, index) => (
          <ListItem key={text} disablePadding>
            <ListItemButton>
              <ListItemText primary={text} />
              <Badge badgeContent={""} color="success" sx={{
                marginRight: "130px"
              }}></Badge>
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <div>
      <Button onClick={toggleDrawer(true)} sx={{
        textTransform: "none",
        color: "green"
      }}>Online users&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <Badge badgeContent={4} color="success">
        </Badge>
        </Button>
      <Drawer open={open} onClose={toggleDrawer(false)}>
        {DrawerList}
      </Drawer>
    </div>
  );
}
